const express = require('express')

const app = express()


app.get('/', (req, res) => {
  res.send('resposta')
})

const PORT = process.env.PORT || 3000

const server = app.listen(PORT, () => { console.log(`PORTA: ${PORT}`)})

const stop = () => {
  server.close()
}
module.exports = server
module.exports.stop = stop